<?php

namespace App\Http\Controllers;

use App\Joke;
use Illuminate\Http\Request;
use JWTAuth;

class JokeController extends Controller
{
    /**
    * @SWG\Get(
    *     path="/jokes",
    *     summary="Get Jokes",
    *     tags={"jokes"},
    *     security={},
    *
    *     @SWG\Response(
    *          response="200",
    *          description="Jokes object"
    *     )
    * )
    */
    public function index() {
      $jokes = Joke::where('status', 'approved')->paginate();

      return response()->json($jokes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = JWTAuth::parseToken()->authenticate();

      $validator = $this->validate($request, [
          'jokeText' => 'required',
      ]);

      $joke = $user->jokes()->create([
          'jokeText' => $request->get('jokeText'),
          'image' => ($request->file('image')) ? $request->file('image')->store(null, 'uploads') : null,
      ]);

      return response()->json($joke);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Joke  $joke
     * @return \Illuminate\Http\Response
     */
    public function show(Joke $joke)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Joke  $joke
     * @return \Illuminate\Http\Response
     */
    public function edit(Joke $joke)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Joke  $joke
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Joke $joke)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Joke  $joke
     * @return \Illuminate\Http\Response
     */
    public function destroy(Joke $joke)
    {
        //
    }
}

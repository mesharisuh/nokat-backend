<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Auth;
use Session;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller
{

    use SendsPasswordResetEmails;

    /**
     * @SWG\Post(
     *     path="/register",
     *     summary="Register user",
     *     tags={"authentication"},
     *     security={},
     *
     *     @SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          description="User's name",
     *          required=true,
     *          type="string",
     *     ),
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="User's email",
     *          required=true,
     *          type="string",
     *          format="email",
     *     ),
     *     @SWG\Parameter(
     *          name="gender",
     *          in="formData",
     *          description="User's gender",
     *          required=true,
     *          type="string",
     *          enum={"male", "female"}
     *     ),
     *     @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          description="User's password",
     *          required=true,
     *          type="string",
     *          format="password"
     *     ),
     *
     *     @SWG\Response(
     *          response="200",
     *          description="User created"
     *     ),
     *     @SWG\Response(
     *          response="422",
     *          description="Unprocessable Entity"
     *     ),
     *     @SWG\Response(
     *          response="500",
     *          description="Could't Create Token"
     *     ),
     * )
     */
    public function register(Request $request) {

        $validator = $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|between:6,32',
        ]);

        $user = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
        ]);

        try {
            if (! $token = JWTAuth::fromUser($user)) {
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Invalid Credentials'
                ], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                'status'    => 'error',
                'error'     => 'Could\'t Create Token'
            ], 500);
        }

        $user->token = $token;

        return response()->json($user);
    }

    /**
     * @SWG\Post(
     *     path="/login",
     *     summary="Login user",
     *     tags={"authentication"},
     *     security={},
     *
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="User's email",
     *          required=true,
     *          type="string",
     *     ),
     *     @SWG\Parameter(
     *          name="password",
     *          in="formData",
     *          description="User's password",
     *          required=true,
     *          type="string",
     *          format="password",
     *     ),
     *
     *     @SWG\Response(
     *          response="200",
     *          description="Logged in"
     *     ),
     *     @SWG\Response(
     *          response="500",
     *          description="Couldn't create token"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="User not found"
     *     )
     * )
     */
    function login(Request $request) {
        $credentials = $request->only('email', 'password');
        $token = '';
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Invalid Credentials'
                ], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                'status'    => 'error',
                'error'     => 'Could\'t Create Token'
            ], 500);
        }

        $user = Auth::user();
        $user->token = $token;

        return response()->json($user);
    }

    /**
     * @SWG\Post(
     *     path="/post-login",
     *     summary="Send user specific data after login",
     *     tags={"authentication"},
     *
     *     @SWG\Parameter(
     *          name="device_token",
     *          in="formData",
     *          description="Device registeration token",
     *          required=true,
     *          type="string",
     *     ),
     *
     *     @SWG\Response(
     *          response="200",
     *          description="Registeration token added"
     *     ),
     * )
     */
    public function postLogin(Request $request) {
        $user = JWTAuth::parseToken()->authenticate();

        $user->device_token = $request->get('device_token');

        $user->save();

        return response()->json($user);
    }

    /**
     * @SWG\Post(
     *     path="/reset-password",
     *     summary="Reset password",
     *     tags={"authentication"},
     *     security={},
     *
     *     @SWG\Parameter(
     *          name="email",
     *          in="formData",
     *          description="User's email",
     *          required=true,
     *          type="string",
     *     ),
     *
     *     @SWG\Response(
     *          response="200",
     *          description="Reset email sent"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="User not found"
     *     ),
     *     @SWG\Response(
     *          response="400",
     *          description="Couldn't send reset email"
     *     ),
     * )
     */
    public function resetPassword(Request $request) {
        $user = User::where('email', $request->get('email'));

        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        if($response != Password::RESET_LINK_SENT) {
            return response()->json('Couldn\'t send reset password email', 400);
        }

        return response()->json('Reset email has been sent');
    }


    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}

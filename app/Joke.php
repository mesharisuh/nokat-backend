<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Joke extends Model
{
  protected $fillable = [
      'jokeText', 'image'
  ];

  protected $appends = [
      'image_link'
  ];

  public function getImageLinkAttribute() {
      return getenv('APP_URL') . '/uploads/' . $this->image;
  }

}
